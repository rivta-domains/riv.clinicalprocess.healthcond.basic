<?xml version="1.0" encoding="utf-8"?>
<!--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements. See the NOTICE file
distributed with this work for additional information
regarding copyright ownership. Sveriges Kommuner och Landsting licenses this file
to you under the Apache License, Version 2.0 (the
        "License"); you may not use this file except in compliance
with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
-->
<?xml-stylesheet type="text/xsl" href="./../TK-doc-sv.xsl"?>
<testsuite>
	<id>GetObservations 2.0</id>
	<contractName>GetObservations</contractName>
	<description>Denna testsvit används för att verifiera implementationen av GetObservations 2.0 inför integration med nationell tjänsteplattform.</description>
	<globaldata>
		<!-- Adress till SIT-Miljön -->
		<!-- <webServiceUrl>https://test.esb.ntjp.se/vp/clinicalprocess/healthcond/basic/GetObservations/2/rivtabp21</webServiceUrl> -->
		<!-- Adress till QA-Miljön -->
		<!-- <webServiceUrl>https://qa.esb.ntjp.se/vp/clinicalprocess/healthcond/basic/GetObservations/2/rivtabp21</webServiceUrl> -->
		<webServiceUrl>http://localhost:8088/mockGetObservationsResponderBinding</webServiceUrl>
		<logicalAddress>SE1234-SY</logicalAddress>
		<personPatientIdRoot>1.2.752.129.2.1.3.1</personPatientIdRoot>
		<personPatientIdExtension>195709263080</personPatientIdExtension>
		<httpHeaderHsaId>consumer</httpHeaderHsaId>
		<sourceSystemHSAIdRoot>1.2.752.129.2.1.4.1</sourceSystemHSAIdRoot>
		<sourceSystemHSAIdExtension>SE1234-SY</sourceSystemHSAIdExtension>
		<!-- Loggparametrar-->
		<logTestData>false</logTestData>
		<logTestDataPath>C:/temp/SOAP-UI/</logTestDataPath>
		<logTestDataFilesAllowed>500</logTestDataFilesAllowed>
		<timeStart>19700101000000</timeStart>
	</globaldata>
	<testcase id="1.1.1 Personnummer">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension>198901302466</personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="1.1.2 Samordningsnummer">
		<data>
			<personPatientIdExtension>196812732391</personPatientIdExtension>
			<personPatientIdRoot>1.2.752.129.2.1.3.3</personPatientIdRoot>
		</data>
	</testcase>
	<testcase id="1.1.3 Lokalt reservnummer">
		<data>
			<personPatientIdExtension>00570926AB80</personPatientIdExtension>
			<personPatientIdRoot>1.2.752.74.9.1</personPatientIdRoot>
		</data>
	</testcase>
	<testcase id="1.2.1 TimeObservationstidpunkter">
		<description>Tidsfiltrering. Sökning sker på alla start- och sluttidpunkter för observationer i det ofiltrerade svaret.
		</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="1.2.2 TimeIntervall">
		<description>Tidsfiltrering. Sökning sker på slumpvisa intervall där observationstidpunkter förekommer i det ofiltrerade svaret.<br/>
			<b>numberOfIntervals</b> anger antal intervall som skapas.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<numberOfIntervals>30</numberOfIntervals>
		</data>
	</testcase>
	<testcase id="1.2.3 TimeTomtSvar">
		<description>Tidsfiltrering. Sökning sker på datum före alla tidpunkter i det ofiltrerade svaret, så att förväntat svar är tomt (inga informationsposter).
		</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="1.2.4 TimeManuell">
		<description>Tidsfiltrering. Sökintervall sätts manuellt. Detta kan användas för utforskande testning och felsökning.<br/>
			<b>timePeriodStart</b> är sökintervallets starttidpunkt (format ÅÅÅÅMMDDttmmss).<br/>
			<b>timePeriodEnd</b> är sökintervallets sluttidpunkt (format ÅÅÅÅMMDDttmmss).
		</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<timePeriodStart>20160101000000</timePeriodStart>
			<timePeriodEnd>20161231235959</timePeriodEnd>
		</data>
	</testcase>
	<testcase id="1.2.5 TimeStart">
		<description>Tidsfiltrering. Sökning sker på slumpvisa intervall där observationstidpunkter förekommer i det ofiltrerade svaret.<br/>
			<b>numberOfIntervals</b> anger antal intervall som skapas.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<numberOfIntervals>30</numberOfIntervals>
		</data>
	</testcase>
	<testcase id="1.2.6 TimeEnd">
		<description>Tidsfiltrering. Sökning sker på slumpvisa intervall där observationstidpunkter förekommer i det ofiltrerade svaret.<br/>
			<b>numberOfIntervals</b> anger antal intervall som skapas.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<numberOfIntervals>30</numberOfIntervals>
		</data>
	</testcase>
	<testcase id="1.3 CareUnit">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<careUnitIdRoot>1.2.752.129.2.1.4.1</careUnitIdRoot>
			<careUnitIdExtension>SE1234-VE</careUnitIdExtension>
		</data>
	</testcase>
	<testcase id="1.4 SourceSystem">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="1.6 CareGiver">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<careGiverIdRoot>1.2.752.129.2.1.4.1</careGiverIdRoot>
			<careGiverIdExtension>SE1234-VG</careGiverIdExtension>
		</data>
	</testcase>
	<testcase id="1.7 SoapFault">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="1.8 ObservationType">
		<description>Filtrering. Verifierar att resultatet endast innehåller poster med given kod för observationstyp.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<observationTypeCode>439401001</observationTypeCode>
			<observationTypeCodeSystem>1.2.752.116.2.1.1</observationTypeCodeSystem>
		</data>
	</testcase>
	<testcase id="1.9 ObservationId">
		<description>Filtrering. Verifierar att resultatet endast innehåller poster med givet observations-id.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<observationIdRoot>SE1234</observationIdRoot>
			<observationIdExtension>1234567</observationIdExtension>
		</data>
	</testcase>
	<testcase id="1.10 ObservationCodedValue">
		<description>Filtrering. Verifierar att resultatet endast innehåller poster med givet kodat värde som är utfallet av en observation.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<observationCodedValueCode>233604007</observationCodedValueCode>
			<observationCodedValueCodeSystem>1.2.752.116.2.1.1</observationCodedValueCodeSystem>
		</data>
	</testcase>
	<testcase id="1.11 ObservationStatus">
		<description>Filtrering. Verifierar att resultatet endast innehåller poster med observationer som har given status.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<observationStatusCode>410515003</observationStatusCode>
			<observationStatusCodeSystem>1.2.752.116.2.1.1</observationStatusCodeSystem>
		</data>
	</testcase>
	<testcase id="1.12 CareProcessId">
		<description>Filtrering. Testa att urval kan göras med 'id' för vårdprocess.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<careProcessId>c07369e3-7605-461a-a57e-7fe20af1bd9e</careProcessId>
		</data>
	</testcase>
	<testcase id="1.13 Relation_ReferedInformationId_Observation">
		<description>Filtrering. Verifierar att resultatet endast innehåller poster som har given relation.<br/>
			<b>relationIdRoot</b> är HSA-id för källsystemet där den relaterade aktiviteten eller observationen finns.<br/>
			<b>relationIdExtension</b> är den källsystems-unika unika id-beteckningen för den relaterade observationen.<br/>
			<b>referredInformationCategorization</b> är typ av uppgift som pekas ut <b>"chb-o"</b> för observation.
		</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<referredInformationIdRoot>SE1234</referredInformationIdRoot>
			<referredInformationIdExtension>12345</referredInformationIdExtension>
			<referredInformationCategorization>chb-o</referredInformationCategorization>
			<!-- <referredInformationCategorization>caa-ga</referredInformationCategorization> -->
		</data>
	</testcase>
	<testcase id="1.14 Relation_ReferedInformationId_Aktivitet">
		<description>Filtrering. Verifierar att resultatet endast innehåller poster som har given relation.<br/>
			<b>relationIdRoot</b> är HSA-id för källsystemet där den relaterade aktiviteten eller observationen finns.<br/>
			<b>relationIdExtension</b> är den källsystems-unika unika id-beteckningen för den relaterade aktiviteten.<br/>
			<b>referredInformationCategorization</b> är typ av uppgift som pekas ut <b>"chb-a"</b> för aktivitet.
		</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<referredInformationIdRoot>SE1234</referredInformationIdRoot>
			<referredInformationIdExtension>12345</referredInformationIdExtension>
			<referredInformationCategorization>chb-a</referredInformationCategorization>
			<!-- <referredInformationCategorization>caa-ga</referredInformationCategorization> -->
		</data>
	</testcase>	
	<testcase id="1.15 Relation_RelationType_Observation">
		<description>Filtrering. Verifierar att resultatet endast innehåller poster som har en relation av en viss typ.<br/>
			<b>relationTypeCode</b> är kod för relationens typ.<br/>
			<b>relationTypeCodeSystem</b> är kodsystemet för den angivna relationskoden.<br/>
			<b>referredInformationCategorization</b> är typ av uppgift som pekas ut <b>"chb-o"</b> för observation.
		</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<relationTypeCode>53241000052107</relationTypeCode>
			<relationTypeCodeSystem>1.2.752.116.2.1.1</relationTypeCodeSystem>
			<referredInformationCategorization>chb-o</referredInformationCategorization>
		</data>
	</testcase>
	<testcase id="1.16 Relation_RelationType_Aktivitet">
		<description>Filtrering. Verifierar att resultatet endast innehåller poster som har en relation av en viss typ.<br/>
			<b>relationTypeCode</b> är kod för relationens typ.<br/>
			<b>relationTypeCodeSystem</b> är kodsystemet för den angivna relationskoden.<br/>
			<b>referredInformationCategorization</b> är typ av uppgift som pekas ut <b>"chb-a"</b> för aktivitet.
		</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
			<relationTypeCode>53241000052107</relationTypeCode>
			<relationTypeCodeSystem>1.2.752.116.2.1.1</relationTypeCodeSystem>
			<referredInformationCategorization>chb-a</referredInformationCategorization>
		</data>
	</testcase>
	<testcase id="2.1 EncodingHeaderProlog">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="2.2 EncodingSpecialCharacters">
		<description>Verifierar att tjänsteproducenten klarar att kapsla in specialtecken på ett korrekt sätt.<br/>
			I filen <b>data.xml</b> finns en parameter "testString". Denna innehåller värdet "&lt;![CDATA[åäö&lt;&gt;&gt;&lt;&amp;]]&gt;".<br/>
			Registrera endast "åäö&lt;&gt;&gt;&lt;&amp;" i källsystemet ("&lt;![CDATA[" och "]]&gt;" krävs för att data.xml inte ska tolka strängen felaktigt.)<br/><br/>
			Det rekommenderas att testa med specialtecken i olika fält som mappas in i olika element i responsen.<br/>
			Testa även gärna andra specialtecken än just dessa, speciellt om ni vet om att vissa specialtecken är extra vanliga i er verksamhet.<br/><br/>
			OBS! Elementet <b>clinicalDocumentNoteText</b> kan leverera <b>DocBook</b>-formatterad text. För tester som rör detta, använd testfall <b>5.14</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <patientId></patientId> -->
			<testString><![CDATA[?åäö<>><&]]></testString>
		</data>
	</testcase>
	<testcase id="3.1 VgVe">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="3.2 RegistrationTime">
		<description>Verifierar att en av de returnerade posterna innehåller dokumentationstidpunkt, dvs när uppgiften registrerades i patientens journal. Element <b>registrationTime</b></description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="4.1 ApprovedForPatientTrue">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="4.2 ApprovedForPatientFalse">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.1 Signed">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.2 Unsigned">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.3 Locked">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.4 ParticipationHealthcareProfessional">
		<description>Verifierar att tjänsteproducenten kan returnera en post där en deltagare är en hälso- och sjukvårdspersonal. Element <b>participation.healthcareProfessional</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.5 ParticipationPatient">
		<description>Verifierar att tjänsteproducenten kan returnera en post där en deltagare är en patient. Element <b>participation.patient</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.6 ParticipationOtherPerson">
		<description>Verifierar att tjänsteproducenten kan returnera en post där en deltagare är en annan person. Element <b>participation.otherPerson</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.7 ParticipationLocationRole">
		<description>Verifierar att tjänsteproducenten kan returnera en post där en deltagare är en plats eller en platsroll. Element <b>participation.locationRole</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.8 ParticipationResource">
		<description>Verifierar att tjänsteproducenten kan returnera en post där en deltagare är en resurs. Element <b>participation.resource</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.9 ParticipationOrganisation">
		<description>Verifierar att tjänsteproducenten kan returnera en post där en deltagare är en organisation. Element <b>participation.organisation</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.10 StatusKänd">
		<description>Testa att status <b>"känd förekomst"</b> kan produceras.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.11 StatusMål">
		<description>Testa att status <b>"mål"</b> kan produceras.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.12 StatusRiskzon">
		<description>Testa att status <b>"riskzon"</b> kan produceras.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.13 ValueNegationTrue">
		<description>Testa att elementet <b>valueNegation</b> kan produceras med värdet <b>true</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.14 ValueNegationFalse">
		<description>Testa att elementet <b>valueNegation</b> kan produceras med värdet <b>false</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.15 ObservationDescription">
		<description>Verifierar att tjänsteproducenten kan returnera en post med en fritextbeskrivning av observationen som kompletterar kodbeteckningen. Element <b>observation.description</b>.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.16.1 Person Patient">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension>198901302466</personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.16.2 Person HealthcareProfessional">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension>198901302466</personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.16.3 Person OtherPerson">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension>198901302466</personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.16.4 Person Participation">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension>198901302466</personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.17.1 Sambandstyp Ersätter">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.17.2 Sambandstyp Refererar Till">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.17.3 Sambandstyp Har Orsak">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.17.4 Sambandstyp Verkställer">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.17.5 Sambandstyp Har Grund">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.17.6 Sambandstyp Är Resultat Av">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.17.7 Sambandstyp Består Av">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>
	<testcase id="5.17.8 Sambandstyp Har Villkor">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <personPatientIdExtension></personPatientIdExtension> -->
		</data>
	</testcase>

	<testcase id="6.1 Loadtest">
		<data>
			<personPatientId1>196001209136</personPatientId1>
			<personPatientId2>195709263080</personPatientId2>
		</data>
	</testcase>
	<testcase id="6.2 Recovery">
		<data>
			<personPatientId1>196001209136</personPatientId1>
			<personPatientId2>195709263080</personPatientId2>
		</data>
	</testcase>
	<testcase id="7.1 CareUnit_Blacklisted">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <patientId></patientId> -->
			<httpHeaderHsaId2>444</httpHeaderHsaId2>
			<filterString>Bortfiltrerad VC</filterString>
		</data>
	</testcase>
	<testcase id="7.2 Consumer_Blacklisted">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <patientId></patientId> -->
			<httpHeaderHsaId2>444</httpHeaderHsaId2>
		</data>
	</testcase>
	<testcase id="7.3 ConsumerIndependent">
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <patientId></patientId> -->
			<httpHeaderHsaId2>444</httpHeaderHsaId2>
		</data>
	</testcase>
	<section id="8 Testdata">
		<description>Då heltäckande testdata kan vara svår att framställa finns i denna testsvit stöd för att säkerställa att testdata är tillräckligt varierad. Detta för att kvalitetssäkringen av tjänstekontraktet skall bli så bred som möjligt.
        </description>
	</section>
	<testcase id="8.1 TimeStart">
		<description>Verifierar att det finns testdata med starttidpunkt där sluttidpunkt saknas.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <patientId></patientId> -->
		</data>
	</testcase>
	<testcase id="8.2 TimeStart_TimeEnd_Lika">
		<description>Verifierar att det finns testdata med både starttidpunkt och sluttidpunkt som är lika.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <patientId></patientId> -->
		</data>
	</testcase>
	<testcase id="8.3 TimeStart_TimeEnd_Olika">
		<description>Verifierar att det finns testdata med både starttidpunkt och sluttidpunkt som är olika.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <patientId></patientId> -->
		</data>
	</testcase>
	<testcase id="8.4 TimeEnd">
		<description>Verifierar att det finns testdata med sluttidpunkt där starttidpunkt saknas.</description>
		<data>
			<!-- Avkommentera för att skriva över den globalt definierade parametern -->
			<!-- <patientId></patientId> -->
		</data>
	</testcase>
</testsuite>
